package com.whereta.dao.impl;

import com.whereta.dao.IDepartmentAccountDao;
import com.whereta.mapper.DepartmentAccountMapper;
import com.whereta.model.DepartmentAccount;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @author Vincent
 * @time 2015/9/2 10:20
 */
@Repository("departmentAccountDao")
public class DepartmentAccountDaoImpl implements IDepartmentAccountDao {
    @Resource
    private DepartmentAccountMapper departmentAccountMapper;

    /**
     * 根据账号id获取部门id
     * @param accountId
     * @return
     */
    public Integer getDepIdByAccountId(int accountId) {
        return departmentAccountMapper.getDepIdByAccountId(accountId);
    }

    /**
     * 根据部门id删除dep-account
     * @param depId
     * @return
     */
    public int deleteByDepId(int depId) {
        return departmentAccountMapper.delete(null,depId);
    }
    /**
     * 根据账号id删除dep-account
     * @param accountId
     * @return
     */
    public int deleteByAccountId(int accountId) {
        return departmentAccountMapper.delete(accountId,null);
    }
    //保存部门-账号
    @Override
    public int save(DepartmentAccount departmentAccount) {
        return departmentAccountMapper.insertSelective(departmentAccount);
    }
}
