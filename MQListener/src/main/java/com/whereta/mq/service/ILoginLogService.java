package com.whereta.mq.service;

import com.whereta.mq.model.LoginLog;
import com.whereta.mq.vo.ResultVO;

/**
 * Created by vincent on 15-9-10.
 */
public interface ILoginLogService {

    ResultVO saveLoginLog(LoginLog loginLog);
}
